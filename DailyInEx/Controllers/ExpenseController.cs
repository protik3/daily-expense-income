﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyInEx.Enum;
using DailyInEx.Models;
using DailyInEx.Persistance;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DailyInEx.Controllers
{
    [Route("api/expense_entry")]
    [ApiController]
    public class ExpenseController : ControllerBase
    {
        private readonly ProjectDbContext _context;

        public ExpenseController(ProjectDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("GetExpenseData")]
        public IEnumerable<Expense> GetExpense()
        {
            var expense = new List<Expense>();
            var companyId = HttpContext.Session.GetInt32("companyId");
            if (companyId != null)
            {
                expense = _context.Expenses.Where(x => x.CompanyId == companyId && x.Status == (int)ApprovalStatus.Pending).ToList();
                return expense;
            }

            return expense;
        }




        [HttpPost]
        public IActionResult ExpenseEntry([FromBody]Expense expense)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            expense.CompanyId = HttpContext.Session.GetInt32("companyId");
            expense.Status = (int)ApprovalStatus.Pending;
            _context.Expenses.Add(expense);
            _context.SaveChanges();
            return CreatedAtAction(nameof(GetItem), new { id = expense.ExpenseId }, expense);

        }

        [HttpGet("{id}")]
        public ActionResult<Expense> GetItem(long id)
        {
            var todoItem = _context.Expenses.Find(id);

            if (todoItem == null)
            {
                return NotFound();
            }

            return todoItem;
        }



        [HttpPut]
        [Route("approve")]
        public IActionResult PutProduct(ApprovalModel approvalModel)
        {
            var companyId = HttpContext.Session.GetInt32("companyId");
            if (approvalModel.Features.Count == 0 || companyId == null)
            {
                return NotFound();
            }
            else
            {
                foreach (var aa in approvalModel.Features)
                {
                    var item = _context.Expenses.SingleOrDefault(x => x.ExpenseId == aa && x.CompanyId == companyId);
                    if (item != null) item.Status = (int)ApprovalStatus.Approve;
                }

                _context.SaveChanges();
            }

            return Ok();

        }

    }
}