﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyInEx.Enum;
using DailyInEx.Models;
using DailyInEx.Persistance;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DailyInEx.Controllers
{
    [Route("api/income_entry")]
    [ApiController]
    public class IncomeController : ControllerBase
    {
        private readonly ProjectDbContext _context;

        public IncomeController(ProjectDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("GetIncomeData")]
        public IEnumerable<Income> GetIncomes()
        {
            var incomes = new List<Income>();
            var companyId = HttpContext.Session.GetInt32("companyId");
            if (companyId != null)
            {
                incomes = _context.Incomes.Where(x => x.CompanyId == companyId && x.Status == (int)ApprovalStatus.Pending).ToList();
                return incomes;
            }

            return incomes;
        }
        [HttpPost]
        public IActionResult IncomeEntry([FromBody]Income income)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            income.CompanyId = HttpContext.Session.GetInt32("companyId");
            income.Status = (int)ApprovalStatus.Pending;
            _context.Incomes.Add(income);
            _context.SaveChanges();
            return CreatedAtAction(nameof(GetItem), new { id = income.IncomeId }, income);

        }

        [HttpGet("{id}")]
        public ActionResult<Income> GetItem(long id)
        {
            var todoItem = _context.Incomes.Find(id);

            if (todoItem == null)
            {
                return NotFound();
            }

            return todoItem;
        }




        [HttpPut]
        [Route("approve")]
        public IActionResult PutProduct(ApprovalModel approvalModel)
        {
            var companyId = HttpContext.Session.GetInt32("companyId");
            if (approvalModel.Features.Count==0 || companyId==null)
            {
                return NotFound();
            }
            else
            {
                foreach (var aa in approvalModel.Features)
                {
                    var item = _context.Incomes.SingleOrDefault(x => x.IncomeId == aa && x.CompanyId == companyId);
                    if (item != null) item.Status = (int) ApprovalStatus.Approve;
                }

                _context.SaveChanges();
            }

            return Ok();
            
        }



    }
}