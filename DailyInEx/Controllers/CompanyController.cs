﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using DailyInEx.Models;
using DailyInEx.Persistance;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DailyInEx.Controllers
{
    [Route("api/company_registration")]   
    public class CompanyController : ControllerBase
    {
        private readonly ProjectDbContext _context;

        public CompanyController(ProjectDbContext context)
        {
            _context = context;
        }
        [HttpGet]
        [Route("GetAllCountry")]
        public  IEnumerable<Country> GetCountries()
        {
            var countries = _context.Countries.ToList();
            return countries;
        }

        [HttpGet]
        [Route("GetAllBanks")]
        public IEnumerable<Bank> GetAllBanks()
        {
            var banks = _context.Banks.ToList();
            return banks;
        }
        [HttpGet("{id}")]
        public ActionResult<Company> GetItem(long id)
        {
            var todoItem =  _context.Companies.Find(id);

            if (todoItem == null)
            {
                return NotFound();
            }

            return todoItem;
        }

        [HttpPost]
        public IActionResult RegistrationCompany([FromBody]Company company)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
                _context.Companies.Add(company);
                _context.SaveChanges();
                return CreatedAtAction(nameof(GetItem), new { id = company.CompanyId }, company);
                
        }
    }
}