import { Component, OnInit } from '@angular/core';
import Companyregistrationserviceservice = require("../Services/company-registration-service.service");
import CompanyRegistrationServiceService = Companyregistrationserviceservice.CompanyRegistrationServiceService;
import Forms = require("@angular/forms");
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ICompanyReg } from "../Module/ICompanyReg";
@Component({
  selector: 'app-company-registration-form',
  templateUrl: './company-registration-form.component.html',
  styleUrls: ['./company-registration-form.component.css']
})
export class CompanyRegistrationFormComponent implements OnInit {
  countries: any[];
  
  registerForm: FormGroup;
  submitted = false;
  company: ICompanyReg = {
    companyId: 0,
    companyName: '',  
    companyEmail: '',
    password: '',
    address: '',
    countryId: null
  };
  constructor(
   
    private formBuilder: FormBuilder,
    private companyRegSer: CompanyRegistrationServiceService) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      companyName: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
      country: ['', Validators.required],
      address: ['', Validators.required]
      
    }, {
      validator: MustMatch('password', 'confirmPassword')
      });

    this.companyRegSer.getCounties().subscribe(
      countries => this.countries = countries
    );
  }
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    var result$ = this.companyRegSer.create(this.company);
    result$.subscribe(company => {
      alert("Data successfully save");
      location.reload();
    });
    
  }
}

 function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.hasOwnProperty) {
    
      return;
    }

    
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  }
}
