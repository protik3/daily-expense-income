import { Component, OnInit } from '@angular/core';
import { IIncomeModel } from "../Module/ICompanyReg";
import { DecimalCheckDirective } from "../Module/decimal-check.directive";
import Companyregistrationserviceservice = require("../Services/company-registration-service.service");
import CompanyRegistrationServiceService = Companyregistrationserviceservice.CompanyRegistrationServiceService;

@Component({
  selector: 'app-income-entry',
  templateUrl: './income-entry.component.html',
  styleUrls: ['./income-entry.component.css']
})
export class IncomeEntryComponent implements OnInit {
  banks: any[];
  showInputFlg: boolean = false;

  incomeModel: IIncomeModel = {
    amount:null,
    isCashOrCheck: true,
    checkNo: '',
    bankId: null,
    date: new Date(),
    particular:''
  };

  constructor(private companyRegSer: CompanyRegistrationServiceService) { }

  radioValueCheck(x) {
    if (x === 1) {
      this.showInputFlg = true;
    }
    else {
      this.showInputFlg = false;
    }
  }
  ngOnInit() {
    this.companyRegSer.getBanks().subscribe(
      banks => this.banks = banks
    );
  }
  submit() {
    var result$ = this.companyRegSer.incomeEntry(this.incomeModel);
    result$.subscribe(incomeModel => {
     alert("Income Entry Data saved");
     location.reload();
    });
  }
}
