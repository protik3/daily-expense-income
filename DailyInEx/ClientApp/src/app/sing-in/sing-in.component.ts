import { Component, OnInit } from '@angular/core';
import { ILoginModel } from "../Module/ICompanyReg";
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import Companyregistrationserviceservice = require("../Services/company-registration-service.service");
import CompanyRegistrationServiceService = Companyregistrationserviceservice.CompanyRegistrationServiceService;

@Component({
  selector: 'app-sing-in',
  templateUrl: './sing-in.component.html',
  styleUrls: ['./sing-in.component.css']
})
export class SingInComponent implements OnInit {
  loginModel :ILoginModel={
  companyId: 0,
  companyName: '',
  companyEmail: '',
  password: ''  
  };

  constructor(private companyRegSer: CompanyRegistrationServiceService,private router : Router) { }

  ngOnInit() {
  }

  onSubmit() {
    this.companyRegSer.userAuth(this.loginModel).subscribe((data: any) => {
      localStorage.setItem('token', data.token);
      this.router.navigate(['/mainFeatures']);
      
    }
      , (err: HttpErrorResponse) => {
        alert("Log in Fail");
      }
      
    )
}
}
