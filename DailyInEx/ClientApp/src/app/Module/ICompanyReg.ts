export interface ICompanyReg {
  companyId: number;
  companyName: string;
  companyEmail: string;
  password: string;
  address: string;
  countryId:number;
}
export interface ILoginModel {
  companyId: number;
  companyName: string;
  companyEmail: string;
  password:string;
}
export interface IIncomeModel {
  amount: number;
  isCashOrCheck: boolean;
  checkNo: string;
  bankId: number;
  particular: string;
  date:Date;
}
export interface IApprovalModel {
  features: number[];
}
