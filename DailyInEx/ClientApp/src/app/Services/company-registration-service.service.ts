import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import "rxjs/add/operator/map";

@Injectable()
export class CompanyRegistrationServiceService {
 
  constructor(private http: Http) { }

  getCounties() {
    return this.http.get('/api/company_registration/GetAllCountry')
      .map(res => res.json());
  }

  getBanks() {
    return this.http.get('/api/company_registration/GetAllBanks')
      .map(res => res.json());
  }

  getAllPendingIncome() {
    return this.http.get('api/income_entry/GetIncomeData')
      .map(res => res.json());
  }
  getAllPendingExpense() {
    return this.http.get('api/expense_entry/GetExpenseData')
      .map(res => res.json());
  }
  create(ICompanyReg) {
    return this.http.post('api/company_registration', ICompanyReg)
      .map(res => res.json());
  }

  userAuth(ILoginModel) {
    return this.http.post('api/login', ILoginModel)
      .map(res => res.json);
  }
  incomeEntry(incomeData) {
    return this.http.post('api/income_entry', incomeData).map(
      res => res.json()
    );

    
  }
  approveIncome(approvalModel) {
    return this.http.put('api/income_entry/approve', approvalModel).map(
      res => res.json()
    );
  }
  approveExpense(approvalModel) {
    return this.http.put('api/expense_entry/approve', approvalModel).map(
      res => res.json()
    );
  }


  expenseEntry(expenseData) {
    return this.http.post('api/expense_entry', expenseData).map(
      res => res.json()
    );
  }
  //getDemo() {
  //    return this.http.get('aa/aa',
  //      ({ headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('token') }) }) as any).map(res=>res.json());
  //}
}
