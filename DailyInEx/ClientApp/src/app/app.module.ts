import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { CompanyRegistrationFormComponent } from './company-registration-form/company-registration-form.component';
import Companyregistrationserviceservice = require("./Services/company-registration-service.service");
import CompanyRegistrationServiceService = Companyregistrationserviceservice.CompanyRegistrationServiceService;
import { ReactiveFormsModule } from '@angular/forms';
import { SingInComponent } from './sing-in/sing-in.component';
import { MainFeatureComponent } from './main-feature/main-feature.component';
import { IncomeEntryComponent } from './income-entry/income-entry.component';
import { ExpenseEntryComponent } from './expense-entry/expense-entry.component';
import { DecimalCheckDirective } from './Module/decimal-check.directive';
import { IncomeViewComponent } from './income-view/income-view.component';
import { ExpenseViewComponent } from './expense-view/expense-view.component';
import Authguard = require("./Auth/auth.guard");
import AuthGuard = Authguard.AuthGuard;

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    CompanyRegistrationFormComponent,
    SingInComponent,
    MainFeatureComponent,
    IncomeEntryComponent,
    ExpenseEntryComponent,
    DecimalCheckDirective,
    IncomeViewComponent,
    ExpenseViewComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'company-registration/new', component: CompanyRegistrationFormComponent },
      { path: 'signIn/new', component: SingInComponent },
      {
        path: 'mainFeatures',
        children: [
          {
            path: "", component: MainFeatureComponent, canActivate: [AuthGuard]
          },
          {
            path: 'income/new', component: IncomeEntryComponent
          },
          { path: 'expense/new', component: ExpenseEntryComponent },
         

        ]

      },
      { path: 'viewIncome', component: IncomeViewComponent },
      { path: 'viewExpense', component: ExpenseViewComponent },
      { path: 'counter', component: CounterComponent },
      { path: 'fetch-data', component: FetchDataComponent },
    ])
  ],
  providers: [
    CompanyRegistrationServiceService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
