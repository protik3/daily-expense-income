import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html'
  
})
export class NavMenuComponent {
  constructor(private  router : Router) { }
  ngOnInit() {
  }
  logout() {   
    localStorage.removeItem('token');
    this.router.navigate(['/signIn/new']);

  }
}
