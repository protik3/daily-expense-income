import { Component, OnInit } from '@angular/core';
import CompanyReg = require("../Module/ICompanyReg");
import IIncomeModel = CompanyReg.IIncomeModel;
import Companyregistrationserviceservice = require("../Services/company-registration-service.service");
import CompanyRegistrationServiceService = Companyregistrationserviceservice.CompanyRegistrationServiceService;

@Component({
  selector: 'app-expense-entry',
  templateUrl: './expense-entry.component.html',
  styleUrls: ['./expense-entry.component.css']
})
export class ExpenseEntryComponent implements OnInit {
  banks: any[];
  showInputFlg: boolean = false;
  incomeModel: IIncomeModel = {
    amount: null,
    isCashOrCheck: true,
    checkNo: '',
    bankId: null,
    date: new Date(),
    particular: ''
  };
  constructor(private companyRegSer: CompanyRegistrationServiceService) { }

  radioValueCheck(x) {
    if (x === 1) {
      this.showInputFlg = true;
    }
    else {
      this.showInputFlg = false;
    }
  }
  ngOnInit() {
    this.companyRegSer.getBanks().subscribe(
      banks => this.banks = banks
    );
  }
  submit() {
    var result$ = this.companyRegSer.expenseEntry(this.incomeModel);
    result$.subscribe(incomeModel => {
      alert("Expense Entry Data saved");
      location.reload();
    });
  }
}
