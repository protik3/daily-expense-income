import { Component, OnInit } from '@angular/core';
import Companyregistrationserviceservice = require("../Services/company-registration-service.service");
import CompanyRegistrationServiceService = Companyregistrationserviceservice.CompanyRegistrationServiceService;
import { IApprovalModel } from "../Module/ICompanyReg";

@Component({
  selector: 'app-income-view',
  templateUrl: './income-view.component.html',
  styleUrls: ['./income-view.component.css']
})
export class IncomeViewComponent implements OnInit {
 
  approvalModel: IApprovalModel = {
    features:[]
  }
  pendingIncomeList :any[];
  features: any[]=[];
  isRowSelected:boolean=false;
  constructor(private companyRegSer: CompanyRegistrationServiceService) { }

  ngOnInit() {
    this.companyRegSer.getAllPendingIncome().subscribe(
      pendingIncomeList => {
        this.pendingIncomeList = pendingIncomeList;
        console.log(this.pendingIncomeList);
      }
      
  );
  }
  onFeatureToggle(featureId, $event) {
    if ($event.target.checked)
      this.approvalModel.features.push(featureId);
    else {
      var index = this.approvalModel.features.indexOf(featureId);
      this.approvalModel.features.splice(index, 1);
    }
  }
  onClickMe() {
    if (this.approvalModel.features.length > 0) {
      var result$ = this.companyRegSer.approveIncome(this.approvalModel).subscribe();      
      if (result$) {
        alert("Income Successfully Approved");
        location.reload();
      }
    } else {
      alert("No item selected");
    }
   

  }
}
