﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DailyInEx.Models
{
    public class Income
    {
        public int IncomeId { get; set; }
        public int? CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }
        [Required]
        public decimal Amount { get; set; }
        [Required]
        public bool IsCashOrCheck { get; set; }
        public string CheckNo { get; set; }
        public int? BankId { get; set; }
        [ForeignKey("BankId")]
        public virtual Bank Bank { get; set; }
        public string Particular { get; set; }
        public DateTime DateTime { get; set; }
        public int Status { get; set; }

    }
}
