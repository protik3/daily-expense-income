﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DailyInEx.Models
{
    public class ApprovalModel
    {
        public List<int> Features { get; set; }

        public ApprovalModel()
        {
            Features=new List<int>();
        }
    }
}
