﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DailyInEx.Models
{
    public class LoginModel
    {
        public int CompanyId { get; set; }      
        public string CompanyName { get; set; }      
        public string CompanyEmail { get; set; }       
        public string Password { get; set; }
    }
}
