﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DailyInEx.Models
{
    [Table("Companies")]
    public class Company
    {
        [Key]
        public int CompanyId { get; set; }
        [Required]
        [StringLength(255)]
        public string CompanyName { get; set; }
        [Required]
        [StringLength(255)]
        public string CompanyEmail { get; set; }
        [Required]
        [StringLength(255)]
        public string Password { get; set; }
        public string Address { get; set; }
        public Country Country { get; set; }
        [Required]
        public int CountryId { get; set; }
    }
}
