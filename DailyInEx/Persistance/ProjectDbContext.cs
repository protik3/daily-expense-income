﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyInEx.Models;
using Microsoft.EntityFrameworkCore;

namespace DailyInEx.Persistance
{
    public class ProjectDbContext : DbContext
    {
        public DbSet<Country>Countries  { get; set; }
        public DbSet<Company>Companies  { get; set; }
        public DbSet<Bank>Banks  { get; set; }
        public DbSet<Income>Incomes  { get; set; }
        public DbSet<Expense>Expenses  { get; set; }
   
        public ProjectDbContext(DbContextOptions<ProjectDbContext>options):base(options)
        {
            
        }
    }
}
