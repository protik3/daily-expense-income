﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DailyInEx.Migrations
{
    public partial class seedBankData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO Banks (BankName) Values ('Sonali bank')");
            migrationBuilder.Sql("INSERT INTO Banks (BankName) Values ('Dutch Bangla bank')");
            migrationBuilder.Sql("INSERT INTO Banks (BankName) Values ('janata bank')");
            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
