﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DailyInEx.Migrations
{
    public partial class SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO Countries (CountryName) Values ('Bangladesh')");
            migrationBuilder.Sql("INSERT INTO Countries (CountryName) Values ('India')");
            migrationBuilder.Sql("INSERT INTO Countries (CountryName) Values ('Africa')");
            migrationBuilder.Sql("INSERT INTO Countries (CountryName) Values ('Brazil')");
            migrationBuilder.Sql("INSERT INTO Countries (CountryName) Values ('Bhutan')");
            migrationBuilder.Sql("INSERT INTO Countries (CountryName) Values ('Nepal')");
           
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM Countries");
        }
    }
}
