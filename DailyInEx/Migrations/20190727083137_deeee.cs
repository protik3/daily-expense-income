﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DailyInEx.Migrations
{
    public partial class deeee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Demo_Countries_Country1CountryId",
                table: "Demo");

            migrationBuilder.DropIndex(
                name: "IX_Demo_Country1CountryId",
                table: "Demo");

            migrationBuilder.DropColumn(
                name: "Country1CountryId",
                table: "Demo");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Country1CountryId",
                table: "Demo",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Demo_Country1CountryId",
                table: "Demo",
                column: "Country1CountryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Demo_Countries_Country1CountryId",
                table: "Demo",
                column: "Country1CountryId",
                principalTable: "Countries",
                principalColumn: "CountryId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
